import { Component, OnInit, OnDestroy, OnChanges } from '@angular/core';
import { Router, NavigationEnd, ResolveEnd, RoutesRecognized, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {UserService} from '../shared';

@Component({
  selector: 'app-contribution',
  templateUrl: './investors.component.html',
  styleUrls: ['./investors.component.scss']
})

export class InvestorsComponent implements OnInit, OnDestroy {
  activeFilter = '';
  pending = false;
  usersList = [];
  activeInvestorId;
  activeInvestor;
  subscriptions = [];
  searchForm: FormGroup;
  searchField = '';
  searchFirstCall = true;
  searchTimeout;
  scrollBindFunction;
  private page = 1;
  private totalPages = 100;
  statusIconsMap = {
    accepted: 'small-check',
    declined: 'close',
    review: 'download'
  };
  accStatuses = {
    confirmed: 'Confirmed',
    refused: 'Refused',
    checking: 'Manual Checking'
  };
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.searchForm = this.fb.group({
      'search': ['', Validators.required],
    });
    this.subscriptions.push(this.route.url.subscribe(data => {
      if (data[0].parameters && data[0].parameters.filter) {
        delete this.activeInvestorId;
        this.page = 1;
        this.usersList = [];
        this.activeFilter = data[0].parameters.filter || this.activeFilter;
      }
      this.scrollBindFunction = this.getUsersList.bind(this);
      document.addEventListener('scroll', this.scrollBindFunction, false);
      this.getUsersList();
    }));
  }

  getUsersList() {
    if (this.pending || this.totalPages < this.page ||
      !this.activeInvestorId && this.page !== 1 &&
      (document.body.offsetHeight - window.innerHeight - window.pageYOffset > 200)) {
      return;
    }
    this.pending  = true;
    const search = this.searchField || '';
    this.userService.getUserList(this.page, this.activeFilter, search).subscribe(usersData => {
      if (this.page === 1) {
        this.usersList = [];
      }
      usersData.data.forEach(el => {
        // el.step1 = 'small-check';
        // el.step2 = 'small-check';
        // if (el.kycStatus === 'review') {
        //   el.step1 = 'download';
        // } else if (el.kycStatus === 'declined') {
        //   el.step1 = 'check';
        // } else if (el.kycStatus === 'final_review') {
        //   el.step2 = 'download';
        // } else if (el.kycStatus === 'final_declined') {
        //   el.step2 = 'check';
        // }
      });
      this.usersList = this.usersList.concat(usersData.data);
      this.pending = false;
      this.page++;
      this.totalPages = usersData.pages || 1;
      setTimeout(() => {
        this.getUsersList();
      }, 200);
    });
  }

  updateSearch() {
    if (this.searchFirstCall) {
      this.searchFirstCall = false;
      return;
    }
    if (this.searchTimeout) {
      window.clearTimeout(this.searchTimeout);
    }
    this.searchTimeout = setTimeout(() => {
      this.page = 1;
      this.getUsersList();
    }, 500);
  }

  updateInvestor(id) {
    this.activeInvestor = this.usersList.find(el => el._id === id);
  }

  selectInvestor(id) {
    this.activeInvestorId = id;
    this.activeInvestor = this.usersList.find(el => el._id === id);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    document.removeEventListener('scroll', this.scrollBindFunction, false);
  }
}
