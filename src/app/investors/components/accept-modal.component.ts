import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
export interface ModalModel {
  title: string;
  message: string;
}
@Component({
  selector: 'confirm',
  templateUrl: `./accept-modal.component.html`,
  styleUrls: ['./accept-modal.component.scss']
})
export class AcceptModalComponent extends DialogComponent<ModalModel, boolean> implements ModalModel, OnInit {
  title: string;
  message: string;
  constructor(
    dialogService: DialogService) {
    super(dialogService);
  }

  ngOnInit() { }
  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    this.result = true;
    this.close();
  }
}
