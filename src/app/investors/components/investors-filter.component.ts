import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'investors-filter',
  templateUrl: './investors-filter.component.html',
  styleUrls: ['./investors-filter.component.scss']
})

export class InvestorsFilterComponent implements OnInit {
  filters = [{
    url: 0,
    name: 'All investors'
  }, {
    url: 2,
    name: 'Accreditation (step 1)'
  }, {
    url: 1,
    name: 'Accepted (step 1)'
  }, {
    url: 3,
    name: 'Refused (step 1)'
  }, {
    url: 4,
    name: 'Accreditation'
  }, {
    url: 5,
    name: 'Accepted'
  }, {
    url: 6,
    name: 'Refused'
  }];
  constructor() { }

  @Input('activeFilter') activeFilter;

  ngOnInit() { }
}
