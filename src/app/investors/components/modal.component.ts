import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
export interface ModalModel {
  title: string;
  message: string;
}
@Component({
  selector: 'confirm',
  templateUrl: `./modal.component.html`,
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent extends DialogComponent<ModalModel, boolean> implements ModalModel, OnInit {
  title: string;
  message: string;
  refusalForm: FormGroup;
  constructor(
    dialogService: DialogService,
    private fb: FormBuilder) {
    super(dialogService);

  }

  ngOnInit() {
    this.refusalForm = this.fb.group({
      'refusal': ['', Validators.required],
    });
  }
  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    this.result = this.refusalForm.controls.refusal.value;
    this.close();
  }
}
