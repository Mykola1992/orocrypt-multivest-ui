import { Component, OnInit, OnDestroy, Output, Input, EventEmitter } from '@angular/core';
import { ModalComponent } from './modal.component';
import { AcceptModalComponent } from './accept-modal.component';
import { DocumentModalComponent } from '../../shared/modals/document-modal.component';
import { DialogService } from 'ng2-bootstrap-modal';
import { UserService } from '../../shared';

@Component({
  selector: 'app-investor',
  templateUrl: './investor.component.html',
  styleUrls: ['./investor.component.scss']
})
export class InvestorComponent implements OnInit, OnDestroy {
  @Input('investor') investor;
  @Input('investors') investors;
  @Output() investorChange = new EventEmitter<any>();
  @Output() getInvestors = new EventEmitter<any>();
  activeInvestor;
  prevUser;
  nextUser;
  prof;
  operationResult = '';
  operationError = false;
  subscriptions = [];
  constructor(
    private dialogService: DialogService,
    private userService: UserService
  ) {
  }
  ngOnInit() {
    this.updatePage();
  }

  updatePage() {
    this.operationError = false;
    this.operationResult = '';
    this.investors.forEach((el, i) => {
      if (this.investor === el._id) {
        this.activeInvestor = el;
        this.prof = el.profile[0];
        this.prevUser = this.investors[i - 1] && this.investors[i - 1]._id ||
          this.investors[this.investors.length - 1]._id;
        this.nextUser = this.investors[i + 1] && this.investors[i + 1]._id ||
          this.investors[0]._id;
        // load more before the end of the list
        if (this.investors.length - i < 5) {
          this.getInvestors.emit();
        }
      }
    });
  }

  changeUser(id) {
    this.investor = id;
    this.updatePage();
    this.investorChange.emit(id);
  }

  backClick() {
    this.investorChange.emit(false);
  }

  confirmRequest() {
    this.subscriptions.push(this.dialogService.addDialog(AcceptModalComponent, {})
      .subscribe((acceptResult) => {
        // We get dialog result
        if (acceptResult) {
          this.userService.confirmUser({
            email: this.activeInvestor.email
          }).subscribe(() => {
            this.activeInvestor.kycStatus = this.activeInvestor.kycStatus === 'review' ? 'accepted' : 'final_accepted';
            this.operationResult = 'User was accepted!';
          }, this.handleServerError.bind(this));
        }
      }));
  }

  showRefusalForm() {
    this.subscriptions.push(this.dialogService.addDialog(ModalComponent, {})
      .subscribe((refusalMessage) => {
        // We get dialog result
        if (refusalMessage) {
          this.userService.refuseUser({
            email: this.activeInvestor.email,
            reason: refusalMessage
          }).subscribe(() => {
            this.activeInvestor.kycStatus = this.activeInvestor.kycStatus === 'review' ? 'declined' : 'final_declined';
            this.operationResult = 'User was refused!';
          }, this.handleServerError.bind(this));
        }
      }));
  }

  handleServerError(err) {
    this.operationError = true;
    if (typeof(err) === 'string') {
      this.operationResult = err;
    } else if (err && err.errors && err.errors.general) {
      this.operationResult = err.errors.general;
    } else {
      this.operationResult = 'Error!';
    }
  }

  showDocumentModal(type) {
    const obj = {
      title: type,
      message: this.prof._id
    };
    this.subscriptions.push(this.dialogService.addDialog(DocumentModalComponent, obj).subscribe());
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

}
