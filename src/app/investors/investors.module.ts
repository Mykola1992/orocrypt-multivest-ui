import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InvestorsComponent } from './investors.component';
import { InvestorComponent } from './components/investor.component';
import { InvestorsFilterComponent } from './components/investors-filter.component';
import { SharedModule, AuthGuard } from '../shared';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { ModalComponent } from './components/modal.component';
import { AcceptModalComponent } from './components/accept-modal.component';

const investorsRouting: ModuleWithProviders = RouterModule.forChild([{
  path: 'investors',
  component: InvestorsComponent,
  canActivate: [AuthGuard]
}]);

@NgModule({
  imports: [
    investorsRouting,
    SharedModule,
    BootstrapModalModule
  ],
  declarations: [
    InvestorsComponent,
    InvestorComponent,
    InvestorsFilterComponent,
    ModalComponent,
    AcceptModalComponent
  ],
  providers: [
    AuthGuard
  ],
  exports: [
    InvestorsFilterComponent,
    InvestorComponent
  ],
  entryComponents: [
    ModalComponent,
    AcceptModalComponent
  ]
})

export class InvestorsModule { }
