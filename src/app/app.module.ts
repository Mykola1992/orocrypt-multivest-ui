import { ModuleWithProviders, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


import { AppComponent } from './app.component';
import { ContributionModule } from './contribution/contribution.module';
import { AccreditationModule } from './accreditation/accreditation.module';
import { HistoryModule } from './history/history.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { InvestorsModule } from './investors/investors.module';
import { DummyComponent } from './shared/components/dummy.component';
import {
  ApiService,
  AuthGuard,
  SettingsService,
  HeaderComponent,
  MenuComponent,
  JwtService,
  SharedModule,
  UserService,
} from './shared';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const rootRouting: ModuleWithProviders = RouterModule.forRoot([{
  path: 'dummy',
  component: DummyComponent
}, {
  path: '**',
  redirectTo: '/contribution'
}], { useHash: true });

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    rootRouting,
    SharedModule,
    ContributionModule,
    AccreditationModule,
    HistoryModule,
    DashboardModule,
    InvestorsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ApiService,
    AuthGuard,
    SettingsService,
    JwtService,
    UserService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
