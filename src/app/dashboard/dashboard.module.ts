import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { SharedModule, AuthGuard } from '../shared';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import * as highcharts from 'highcharts';

const dashboardRouting: ModuleWithProviders = RouterModule.forChild([{
  path: 'dashboard',
  component: DashboardComponent,
  canActivate: [AuthGuard]
}]);
declare const require: any;

export function highchartsFactory() {
  return highcharts;
}

@NgModule({
  imports: [
    BrowserModule,
    ChartModule,
    dashboardRouting,
    SharedModule
  ],
  declarations: [DashboardComponent],
  providers: [
    AuthGuard, {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    }
  ]
})
export class DashboardModule { }
