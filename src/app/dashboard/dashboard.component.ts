import { Component, OnInit } from '@angular/core';
import {UserService} from '../shared';

@Component({
  selector: 'app-contribution',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  chartOptions;
  remains = 0;
  totalSold = 0;
  soldToday = 0;
  constructor(
    private userService: UserService
  ) {}

  ngOnInit() {
    this.userService.getStats().subscribe(data => {
      this.remains = data.tokensForSale;
      this.totalSold = data.soldTokens;
    });
    this.userService.getDashboard().subscribe(data => {
      const categories = [];
      const chartPoints = [{
        name: 'All Contributions',
        data: [],
        color: '#007AFF'
      }, {
        name: 'Less then 5 000 CNAT',
        data: [],
        color: '#ff0000'
      }, {
        name: 'Less then 10 000 CNAT',
        data: [],
        color: '#52C92E'
      }, {
        name: '10 000 CNAT or More',
        data: [],
        color: '#2B2B2B'
      }];
      const namesMap = ['all', 'less5k', 'less10k', 'other'];
      this.soldToday = data[0] && data[0][namesMap[0]] || 0;
      data.forEach((el) => {
        categories.push(el._id);
        namesMap.forEach((name, i) => {
          chartPoints[i].data.push(el[name]);
        });
      });
      this.chartOptions = {
        chart: {
          backgroundColor: '#fff'
        },
        title: {
          text: ''
        },
        yAxis: {
          title: {
            text: ''
          },
          gridLineColor: '#EDEDED'
        },
        xAxis: {
          categories,
          gridLineWidth: 1,
          gridLineColor: '#EDEDED'
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle',
          itemStyle: {
            color: '#2B2B2B',
            'font-weight': 'normal'
          },
          itemHoverStyle: {
            color: '#818181'
          }
        },
        series: chartPoints,
        backgroundColor: '#ff00ff'
      };
    });
  }

}
