import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../shared';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  constructor(
    private userService: UserService,
  ) {
  }

  ngOnInit() {}

}
