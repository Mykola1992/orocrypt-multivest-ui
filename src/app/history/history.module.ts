import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HistoryComponent } from './history.component';
import { SharedModule, AuthGuard } from '../shared';

const historyRouting: ModuleWithProviders = RouterModule.forChild([{
  path: 'history',
  component: HistoryComponent,
  canActivate: [AuthGuard]
}]);

@NgModule({
  imports: [
    historyRouting,
    SharedModule
  ],
  declarations: [HistoryComponent],
  providers: [
    AuthGuard
  ]
})
export class HistoryModule { }
