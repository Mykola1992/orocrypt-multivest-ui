import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { User } from './shared/models';
import { UserService, SettingsService } from './shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  currentUser: User;
  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    private translate: TranslateService
  ) {
    translate.setDefaultLang('en');
  }

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    // todo: need to fix
    const loginAccess = ['accreditation', 'history', 'contribution'];
    let needRedirect = false;
    loginAccess.forEach(el => {
      if (window.location.href.indexOf(el) !== -1) {
        needRedirect = true;
      }
    });
    this.userService.populate(needRedirect);
    this.settingsService.loadSettingsList();
  }
}
