import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ApiService } from './api.service';

@Injectable()
export class SettingsService {
  private settingsSubject = new BehaviorSubject<any>({});
  public settings = this.settingsSubject.asObservable().distinctUntilChanged();

  constructor (
    private apiService: ApiService
  ) {}

  loadSettingsList() {
    return this.apiService.get('/country/list')
      .map(
        data => {
          const countries = Object.keys(data).map(key => ({
            text: data[key],
            id: key
          }));
          this.settingsSubject.next(Object.assign({}, this.getSettingsList(), {
            countries,
            USStates: this.getUSStates(),
            CAStates: this.getCAStates(),
          }));
          return data;
        }
      ).subscribe();
  }

  getUSStates() {
    return [{'id': 'AL', 'text': 'Alabama'},
      {'id': 'AK', 'text': 'Alaska'},
      {'id': 'AZ', 'text': 'Arizona'},
      {'id': 'AR', 'text': 'Arkansas'},
      {'id': 'CA', 'text': 'California'},
      {'id': 'CO', 'text': 'Colorado'},
      {'id': 'CT', 'text': 'Connecticut'},
      {'id': 'DE', 'text': 'Delaware'},
      {'id': 'DC', 'text': 'District of Columbia'},
      {'id': 'FL', 'text': 'Florida'},
      {'id': 'GA', 'text': 'Georgia'},
      {'id': 'HI', 'text': 'Hawaii'},
      {'id': 'ID', 'text': 'Idaho'},
      {'id': 'IL', 'text': 'Illinois'},
      {'id': 'IN', 'text': 'Indiana'},
      {'id': 'IA', 'text': 'Iowa'},
      {'id': 'KS', 'text': 'Kansas'},
      {'id': 'KY', 'text': 'Kentucky'},
      {'id': 'LA', 'text': 'Louisiana'},
      {'id': 'ME', 'text': 'Maine'},
      {'id': 'MT', 'text': 'Montana'},
      {'id': 'NE', 'text': 'Nebraska'},
      {'id': 'NV', 'text': 'Nevada'},
      {'id': 'NH', 'text': 'New Hampshire'},
      {'id': 'NJ', 'text': 'New Jersey'},
      {'id': 'NM', 'text': 'New Mexico'},
      {'id': 'NY', 'text': 'New York'},
      {'id': 'NC', 'text': 'North Carolina'},
      {'id': 'ND', 'text': 'North Dakota'},
      {'id': 'OH', 'text': 'Ohio'},
      {'id': 'OK', 'text': 'Oklahoma'},
      {'id': 'OR', 'text': 'Oregon'},
      {'id': 'MD', 'text': 'Maryland'},
      {'id': 'MA', 'text': 'Massachusetts'},
      {'id': 'MI', 'text': 'Michigan'},
      {'id': 'MN', 'text': 'Minnesota'},
      {'id': 'MS', 'text': 'Mississippi'},
      {'id': 'MO', 'text': 'Missouri'},
      {'id': 'PA', 'text': 'Pennsylvania'},
      {'id': 'RI', 'text': 'Rhode Island'},
      {'id': 'SC', 'text': 'South Carolina'},
      {'id': 'SD', 'text': 'South Dakota'},
      {'id': 'TN', 'text': 'Tennessee'},
      {'id': 'TX', 'text': 'Texas'},
      {'id': 'UT', 'text': 'Utah'},
      {'id': 'VT', 'text': 'Vermont'},
      {'id': 'VA', 'text': 'Virginia'},
      {'id': 'WA', 'text': 'Washington'},
      {'id': 'WV', 'text': 'West Virginia'},
      {'id': 'WI', 'text': 'Wisconsin'},
      {'id': 'WY', 'text': 'Wyoming'}];
  }

  getCAStates() {
    return [
      {'id': 'AB', 'text': 'Alberta'},
      {'id': 'BC', 'text': 'British Columbia'},
      {'id': 'MB', 'text': 'Manitoba'},
      {'id': 'NB', 'text': 'New Brunswick'},
      {'id': 'NL', 'text': 'Newfoundland and Labrador'},
      {'id': 'NS', 'text': 'Nova Scotia'},
      {'id': 'ON', 'text': 'Ontario'},
      {'id': 'PE', 'text': 'Prince Edward Island'},
      {'id': 'QC', 'text': 'Quebec'},
      {'id': 'SK', 'text': 'Saskatchewan'},
      {'id': 'NT', 'text': 'Northwest'},
      {'id': 'NU', 'text': 'Nunavut'},
      {'id': 'YT', 'text': 'Yukon'}
    ];
  }

  getSettingsList() {
    return this.settingsSubject.value;
  }
}
