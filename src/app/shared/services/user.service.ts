import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
import { User } from '../models';
import { window } from 'rxjs/operators/window';

@Injectable()
export class UserService {
  private currentUserSubject = new BehaviorSubject<any>(new User());
  public currentUser = this.currentUserSubject.asObservable().distinctUntilChanged();

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor (
    private apiService: ApiService,
    private http: Http,
    private jwtService: JwtService,
    private router: Router
  ) {}

  prepareUserObject(profile) {
    profile.initials = profile.firstName.substr(0, 1) + profile.lastName.substr(0, 1);
    profile.fullName = `${profile.firstName} ${profile.lastName}`;
    profile.fullAddress = ((profile.appartment || '') + ' ' + profile.street + ' ' + profile.city + ', ' +
      (profile.state || '') + ' ' + profile.zipCode + ', ' + profile.country)
  }

  setStatuses(user) {
    if (user.kycStatus === 'REVIEW') {
      user.step1 = 'review';
    } else if (user.kycStatus === 'ACCEPTED') {
      user.step1 = 'accepted';
    } else if (user.kycStatus === 'DECLINED') {
      user.step1 = 'declined';
      user.step2 = 'declined';
    } else if (user.kycStatus === 'FINAL_REVIEW') {
      user.step1 = 'accepted';
      user.step2 = 'review';
    } else if (user.kycStatus === 'FINAL_ACCEPTED') {
      user.step1 = 'accepted';
      user.step2 = 'accepted';
    } else if (user.kycStatus === 'FINAL_DECLINED') {
      user.step1 = 'accepted';
      user.step2 = 'declined';
    }
  }

  handleError(err) {
    if (err.status === 401) {
      this.purgeAuth(true);
    }
    return Observable.throw(err);
  }

  populate(redirect) {
    if (this.jwtService.getToken()) {
      let user = {};
      try {
        user = JSON.parse(localStorage.user);
      } catch (err) {}
      this.setAuth(user);
    } else {
      this.purgeAuth(redirect);
    }
  }

  setAuth(user) {
    this.jwtService.saveToken(user.token);
    localStorage.user = JSON.stringify(user);
    user.profile = user.profile || {};
    this.currentUserSubject.next(user);
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth(redirect) {
    this.jwtService.destroyToken();
    localStorage.user = '';
    this.currentUserSubject.next(new User());
    this.isAuthenticatedSubject.next(false);
    if (redirect) {
      this.router.navigateByUrl('/');
    }
  }

  login(credentials): Observable<User> {
    return this.apiService.post('/user/authenticate', credentials)
      .map(
        data => {
          data.user.token = data.token;
          this.setAuth(data.user);
          return data;
        }
      );
  }

  register(credentials): Observable<User> {
    return this.apiService.post('/user/register', credentials)
      .map(
        data => {
          return data;
        }
      );
  }

  confirmAccount(credentials): Observable<User> {
    return this.apiService.post('/user/activate', credentials)
      .map(
        data => {
          return data;
        }
      );
  }

  forgotPassword(credentials): Observable<User> {
    return this.apiService.post('/user/restore-request', credentials)
      .map(
        data => {
          return data;
        }
      );
  }

  resetPassword(credentials): Observable<User> {
    return this.apiService.post('/user/restore', credentials)
      .map(
        data => {
          return data;
        }
      );
  }

  getContribution() {
    return this.apiService.get('/contribution/all')
      .map(
        data => {
          // transform exchangeRates to currency object
          let exchangeCurrencyArr = [];
          const exchangeCurrencyObj = {};
          Object.keys(data.exchangeRates).forEach(el => exchangeCurrencyArr.push(...el.split('2')));
          exchangeCurrencyArr.forEach(el => exchangeCurrencyObj[el] = el);
          exchangeCurrencyArr = Object.keys(exchangeCurrencyObj).map(el => el);
          data.currencis = exchangeCurrencyArr;
          this.currentUserSubject.next(Object.assign(this.getCurrentUser(), data));
          return data;
        }
      )
      .catch(err => this.handleError(err));
  }

  getStats() {
    return this.apiService.get('/ico/stats')
      .map(data => {
        data.startTime = data.startTime * 1000;
        data.endTime = data.endTime * 1000;
        return data;
      })
      .catch(err => this.handleError(err));
  }

  getUserList(page = 1, filter = '', search = '') {
    let str = `/user/list?page=${page}`;
    str += (filter ? `&filter=${filter}` : '');
    str += (search ? `&search=${search}` : '');
    return this.apiService.get(str)
      .map(data => {
        data.data.forEach(el => {
          this.setStatuses(el);
          if (el.kycStatus) {
            el.kycStatus = el.kycStatus.toLowerCase();
          }
          if (el.profile.length) {
            this.prepareUserObject(el.profile[0]);
          }
          el.profile[0] = el.profile[0] || {};
        });
        return data;
      })
      .catch(err => this.handleError(err));
  }

  getAddresses(address) {
    return this.apiService.post(`/ico/addresses/${address}`)
      .map(data => {
        return data;
      })
      .catch(err => this.handleError(err));
  }

  getTokensPerValue(value, tokens) {
    return this.apiService.get(`/ico/tokens/${tokens}?value=${value}`)
      .map(data => {
        return data;
      })
      .catch(err => this.handleError(err));
  }

  getUserBalance() {
    return this.apiService.get(`/user/balance`)
      .map(data => {
        return data;
      })
      .catch(err => this.handleError(err));
  }

  getHistory(id) {
    let link = '/ico/list';
    link += (id ? `?profileId=${id}` : '');
    return this.apiService.get(link)
      .map(data => {
        return data;
      })
      .catch(err => this.handleError(err));
  }

  getDashboard() {
    return this.apiService.get('/ico/dashboard')
      .map(data => {
        return data;
      })
      .catch(err => this.handleError(err));
  }

  getUserImages(id?) {
    const str = id ? `?profileId=${id}` : '';
    return this.apiService.get(`/profile/images${str}`)
      .map(data => {
        return data;
      })
      .catch(err => this.handleError(err));
  }

  getProfile() {
    return this.apiService.get('/profile')
      .map(data => {
        if (data.profile) {
          this.prepareUserObject(data.profile);
        } else {
          data.profile = {};
        }
        this.setStatuses(data);
        const currentUser = Object.assign({}, this.getCurrentUser(), data);
        this.currentUserSubject.next(currentUser);
        return data;
      })
      .catch(err => this.handleError(err));
  }

  postProfile(credentials): Observable<any> {
    return this.apiService.post('/profile', credentials)
      .map(data => {
        this.currentUserSubject.next(Object.assign(this.getCurrentUser(), {
          profile: data.profile
        }));
        return data;
      })
      .catch(err => this.handleError(err));
  }

  confirmUser(obj) {
    return this.apiService.post('/accreditation/accept', obj)
      .map(data => {
        return data;
      })
      .catch(err => this.handleError(err));
  }

  refuseUser(obj) {
    return this.apiService.post('/accreditation/decline', obj)
      .map(data => {
        return data;
      })
      .catch(err => this.handleError(err));
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }
}
