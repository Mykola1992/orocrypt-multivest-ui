import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import { ShowAuthedDirective } from './directives';
import { FileInputComponent, DummyComponent, HistoryListComponent } from './components';
import { DocumentModalComponent } from './modals';
import { FormatNumberPipe } from './format-number.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule
  ],
  declarations: [
    ShowAuthedDirective,
    FileInputComponent,
    HistoryListComponent,
    DummyComponent,
    DocumentModalComponent,
    FormatNumberPipe
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    ShowAuthedDirective,
    FileInputComponent,
    DocumentModalComponent,
    HistoryListComponent,
    DummyComponent,
    FormatNumberPipe,
    TranslateModule
  ],
  entryComponents: [
    DocumentModalComponent
  ]
})
export class SharedModule {}
