import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { UserService } from '../services/user.service';

export interface ModalModel {
  title: string;
  message: string;
}
@Component({
  selector: 'app-document-modal',
  templateUrl: `./document-modal.component.html`,
  styleUrls: ['./document-modal.component.scss']
})
export class DocumentModalComponent extends DialogComponent<ModalModel, boolean> implements ModalModel, OnInit {
  title: string;
  message: string; // ? user id
  image;
  pending = true;
  imageTitleMap = {
    documentScan: 'Identity Document',
    residenceProof: 'Proof of Residence',
    selfie: 'Selfie with ID'
  };
  constructor(
    dialogService: DialogService,
    private userService: UserService
  ) {
    super(dialogService);
  }

  ngOnInit() {
    this.userService.getUserImages(this.message).subscribe(imageData => {
      this.pending = false;
      this.image = imageData[this.title];
    });
  }
}
