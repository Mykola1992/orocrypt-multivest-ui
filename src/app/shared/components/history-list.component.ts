import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss']
})
export class HistoryListComponent implements OnInit, OnChanges {
  @Input() userId;
  tokensData;
  total: number;

  constructor(
    private userService: UserService,
  ) {
  }

  ngOnInit() {
    if (!this.userId) {
      this.updateHistory();
    }
  }

  ngOnChanges() {
    this.updateHistory();
  }

  updateHistory() {
    this.userService.getHistory(this.userId).subscribe(data => {
      let sum = 0;
      if (data.lenght) {
        data.forEach(el => {
          if (this.userId) {
            el.address =
              `${el.address.substr(0, 7)}...${el.address.substr(el.address.length - 5)}`;
          }
          sum += +el.amount;
        });
      }
      this.tokensData = data;
      this.total = sum;
    });
  }

}
