// Dummy component for route changes
import { Component } from '@angular/core';

@Component({
  selector: 'dummy',
  template: ''
})
export class DummyComponent {}
