import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.scss']
})

export class FileInputComponent implements OnInit {
  @Output('onUpdate') onUpdate: EventEmitter<any> = new EventEmitter();
  @Input('fileName') baseFileName;
  fileValue: string;
  fileName = '';
  constructor() {

  }

  ngOnInit() {
    this.fileValue = this.baseFileName;
    this.fileName = this.baseFileName || 'File not selected';
  }

  handleInputChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const reader = new FileReader();

    this.fileName = file.name;
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    const reader = e.target;
    this.fileValue = reader.result;
    this.onUpdate.emit(this.fileValue);
  }
}
