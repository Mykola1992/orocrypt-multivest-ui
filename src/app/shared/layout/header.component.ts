import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from '../services';

@Component({
  selector: 'layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
  activeRoute: string;
  currentUser;
  routeMap = {
    accreditation: {
      icon: 'list'
    },
    contribution: {
      icon: 'plus'
    },
    history: {
      icon: 'docs'
    },
    dashboard: {
      icon: 'dashboard'
    },
    investors: {
      icon: 'investors'
    }
  };

  constructor(
    private userService: UserService,
    private router: Router,
    private translate: TranslateService
  ) {
    translate.get('header').subscribe((res: any) => {
      Object.keys(this.routeMap).forEach(key => {
        this.routeMap[key].name = res[key];
      });
    });
    this.activeRoute = this.router.url.split('/')[1];
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.activeRoute = event.url.split('/')[1].split(';')[0];
        window.document.body.classList.remove('menuOpened');
      }
    });
  }

  ngOnInit() {
    this.currentUser = {
      profile: {}
    };
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
  }

  toggleMenu() {
    window.document.body.classList.toggle('menuOpened');
  }
}
