import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../models';
import { UserService } from '../services';

@Component({
  selector: 'layout-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  currentUser: User;
  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
  }

  closeMenu() {
    window.document.body.classList.remove('menuOpened');
  }

  updateRoute(url) {
    // hack to reload page if page is already selected;
    if (!this.router.url.indexOf(url)) {
      this.router.navigateByUrl('/dummy', { skipLocationChange: true });
      setTimeout(() => this.router.navigate([url]));
    }
  }

  logout() {
    this.userService.purgeAuth(true);
  }
}
