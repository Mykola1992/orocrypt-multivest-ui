import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'formatNumber'})
export class FormatNumberPipe implements PipeTransform {
  transform(value: number): string {
    const res = [];
    const decimal = (value + '').split('.')[1];
    if (decimal) {
      res.unshift('.', decimal);
    }
    let val = (Math.floor(value)) + '';
    let valArr = val.split('').reverse();
    for (let i = 0; i < valArr.length; i++) {
      res.unshift(valArr[i]);
      if (i % 3 === 2) {
        res.unshift(' ');
      }
    }
    return res.join('');
  }
}
