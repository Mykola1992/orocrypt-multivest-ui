import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { UserService, SettingsService } from '../../shared';

@Component({
  selector: 'auth-form',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})

export class AuthComponent implements OnInit {
  serverErrorsCount;
  formFields;
  serverErrors;
  title: String = '';
  isSubmitting = false;
  authForm: FormGroup;
  countriesItems: Array<any>;
  blockTitle: string;
  description: string;
  @Input('form-type') formType: 'login';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private settingsService: SettingsService,
    private fb: FormBuilder,
  ) {}

  ngOnInit() {
    this.formFields = {};
    this.serverErrors = {};
    this.settingsService.settings.subscribe(data => {
      this.countriesItems = data.countries;
    });

    if (this.formType === 'login') {
      this.authForm = this.fb.group({
        'email': ['', Validators.email],
        'password': ['', Validators.required]
      });
    } else if (this.formType === 'register') {

      this.authForm = this.fb.group({
        'email': ['', Validators.email],
        'country': [''],
        'password': ['', Validators.required],
        'retryPassword': ['', Validators.required],
        'agreeRegister': ['', Validators.required]
      });
    } else if (this.formType === 'forgotPassword') {
      this.authForm = this.fb.group({
        'email': ['', Validators.email]
      });
      this.title = 'Forgot Password';
    } else if (this.formType === 'resetPassword') {
      this.authForm = this.fb.group({
        'password': ['', Validators.required],
        'retryPassword': ['', Validators.required]
      });
    }

    if (this.formType === 'login') {
      this.title = 'Login';
    } else if (this.formType === 'register') {
      this.title = 'Register';
    } else if (this.formType === 'forgotPassword') {
      this.blockTitle = 'Password retrieval';
      this.description = 'Please enter the email that you used to create your account.';
      this.title = 'Forgot password';
    } else if (this.formType === 'resetPassword') {
      this.blockTitle = 'Reset password';
      this.description = 'Please enter the new password two times';
      this.title = 'Reset Password';
    }
  }

  submitForm() {
    this.isSubmitting = true;
    const credentials = this.authForm.value;
    if (this.formType === 'login') {
      this.userService.login(credentials).subscribe(data => {
        setTimeout(() => {
          this.router.navigateByUrl('/contribution');
        });
        }, err => this.handleErrors(err)
      );
    } else if (this.formType === 'register') {
      const registerData = {
        email: credentials.email,
        country: credentials.country,
        password: credentials.password
      };
      this.userService.register(registerData).subscribe(
        data => {
          this.router.navigateByUrl('/register/confirm');
        },
        err => this.handleErrors(err)
      );
    } else if (this.formType === 'forgotPassword') {
      this.userService.forgotPassword(credentials).subscribe(
        data => {
          this.router.navigateByUrl('/forgot-password/confirm');
        },
        err => this.handleErrors(err)
      );
    } else if (this.formType === 'resetPassword') {
      this.route.queryParams.subscribe((params: Params) => {
        const resetData = Object.assign({
          password: this.authForm.value.password
        }, params);
        this.userService.resetPassword(resetData).subscribe(
          data => {
            this.router.navigateByUrl('/reset-password/confirm');
          },
          err => {
            if (err.errors && err.errors.email) {
              err.errors.password = err.errors.email;
            }
            this.handleErrors(err);
          }
        );
      });
    }
  }

  handleErrors(err) {
    this.serverErrors = this.serverErrors || {};
    this.isSubmitting = false;
    if (err.errors) {
      Object.keys(err.errors).forEach(key => {
        this.serverErrors[key] = err.errors[key];
      });
    }
  }

  selected(value: any): void {
    this.formFields.country = value.id;
  }

  inputUpdates(field) {
    if (this.serverErrors[field]) {
      delete this.serverErrors[field];
      this.serverErrorsCount = Object.keys(this.serverErrors).length;
    }
  }
}
