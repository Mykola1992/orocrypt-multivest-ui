import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { UserService } from '../../shared';

@Component({
  selector: 'info-form',
  templateUrl: './info-form.component.html',
  styleUrls: ['./info-form.component.scss']
})

export class InfoFormComponent implements OnInit {
  errored: false;
  infoMessage: string;
  title: string;
  @Input('form-type') formType: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    if (this.formType === 'registerConfirm') {
      this.title = 'Registration';
      this.infoMessage = 'A confirmation email has been sent to you. Please confirm your email address to complete registration.';
    } else if (this.formType === 'activateAccount') {
      this.title = 'Activation';
      this.infoMessage = 'Please wait...';
      this.route.queryParams.subscribe((params: Params) => {
        this.userService.confirmAccount(params)
          .subscribe(
            () => {
              this.infoMessage = 'Your email was successfully confirmed, you can log in now.';
            },
            err => {
              if (err && err.errors && err.errors.email) {
                this.infoMessage = err.errors.email;
              } else {
                this.infoMessage = 'Something went wrong, please try again.';
              }
            }
          );
      });
    } else if (this.formType === 'forgotPasswordConfirm') {
      this.title = 'Forgot Password';
      this.infoMessage = 'A letter was sent to your email with instructions for password recovery.';
    } else if (this.formType === 'resetPasswordConfirm') {
      this.title = 'Reset Password';
      this.infoMessage = 'Password has been reset.';
    }
  }
}
