import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SelectModule } from 'ng2-select';
import { ContributionComponent } from './contribution.component';
import { SharedModule, AuthGuard, NoAuthGuard } from '../shared';
import { CountdownTimerComponent } from './components/countdown-timer.component';
import { BalanceComponent } from './components/balance.component';
import { CurrentPriceComponent } from './components/current-price.component';
import { AuthComponent } from './auth/auth.component';
import { InfoFormComponent } from './info-form/info-form.component';

const contributionRouting: ModuleWithProviders = RouterModule.forChild([{
  path: '',
  component: ContributionComponent,
  data: {type: 'main'}
}, {
  path: 'register/confirm',
  component: ContributionComponent,
  data: {type: 'registerConfirm'},
  canActivate: [NoAuthGuard]
}, {
  path: 'activate',
  component: ContributionComponent,
  data: {type: 'activateAccount'},
  canActivate: [NoAuthGuard]
}, {
  path: 'forgot-password',
  component: ContributionComponent,
  data: {type: 'forgotPassword'},
  canActivate: [NoAuthGuard]
}, {
  path: 'forgot-password/confirm',
  component: ContributionComponent,
  data: {type: 'forgotPasswordConfirm'},
  canActivate: [NoAuthGuard]
}, {
  path: 'reset-password',
  data: {type: 'resetPassword'},
  component: ContributionComponent,
  canActivate: [NoAuthGuard]
}, {
  path: 'reset-password/confirm',
  component: ContributionComponent,
  data: {type: 'resetPasswordConfirm'},
  canActivate: [NoAuthGuard]
}, {
  path: 'contribution',
  component: ContributionComponent,
  data: {type: 'main'},
  canActivate: [AuthGuard],
  children: [{
    path: 'buy',
    component: ContributionComponent,
    data: {type: 'buy'},
    canActivate: [AuthGuard]
  }, {
    path: 'buy/success',
    component: ContributionComponent,
    data: {type: 'tokensSuccess'},
    canActivate: [AuthGuard]
  }, {
    path: 'buy/info',
    component: ContributionComponent,
    data: {type: 'buyInfo'},
    canActivate: [AuthGuard]
  }, {
    path: 'buy/:tokens',
    component: ContributionComponent,
    data: {type: 'tokensForm'},
    canActivate: [AuthGuard]
  }]
}]);

@NgModule({
  imports: [
    contributionRouting,
    SharedModule,
    SelectModule
  ],
  declarations: [
    ContributionComponent,
    CountdownTimerComponent,
    BalanceComponent,
    CurrentPriceComponent,
    AuthComponent,
    InfoFormComponent
  ],
  providers: [
    AuthGuard,
    NoAuthGuard
  ]
})
export class ContributionModule { }
