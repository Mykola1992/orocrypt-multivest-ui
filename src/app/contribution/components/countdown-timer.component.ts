import { Component, ElementRef, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'countdown-timer',
  templateUrl: './countdown-timer.component.html',
  styleUrls: ['./countdown-timer.component.scss']
})

export class CountdownTimerComponent implements OnInit, OnDestroy {
  future: Date;
  beginning: Date;
  futureNumber: number;
  beginningNumber: number;
  diff: number;
  $counter: Observable<number>;
  subscription: Subscription;
  lastDateEnd: number;
  timerStatus: string;
  timerText: string;
  progress: number;
  timerData = {
    seconds: '00',
    minutes: '00',
    hours: '00',
    days: '0'
  };
  endedDateData = {
    time: '00:00',
    date: '00',
    year: '2000',
    month: 'jan'
  };
  fullProgress = 565.49;
  @Input('dateStart') dateStart: number;
  @Input('dateEnd') dateEnd: number;
  constructor(
    private elm: ElementRef,
  ) { }

  countData(t) {
    if (t < 0) {
      return {
        days: '0',
        hours: '00',
        minutes: '00',
        seconds: '00'
      }
    }
    let days, hours, minutes, seconds;
    days = Math.floor(t / 86400);
    t -= days * 86400;
    hours = Math.floor(t / 3600) % 24;
    t -= hours * 3600;
    minutes = Math.floor(t / 60) % 60;
    t -= minutes * 60;
    seconds = t % 60;

    return {
      days: days + '',
      hours: ('0' + hours).slice(-2),
      minutes: ('0' + minutes).slice(-2),
      seconds: ('0' + seconds).slice(-2)
    };
  }

  ngOnInit() {
    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December'
    ];
    const timerTick = (x) => {
      const currentTime = +new Date();
      // check if dateEnd was updated
      if (this.dateEnd !== this.lastDateEnd) {
        this.updateDates();
      }
      if (currentTime < this.dateStart) {
        this.timerStatus = 'starts';
        this.timerText = 'Token sale starts';
        this.updateDates(+new Date(this.dateStart - 2592000000), this.dateStart);
      } else if (currentTime < this.dateEnd) {
        this.timerStatus = 'ending';
        this.timerText = 'Token sale ends';
      } else {
        this.timerStatus = 'ended';
        this.timerText = 'Token sale ended';
        // fix to fill circle;
        this.beginningNumber = this.futureNumber;
        this.endedDateData = {
          time: `${('0' + this.future.getHours()).slice(-2)}:${('0' + this.future.getMinutes()).slice(-2)}`,
          date: ('0' + this.future.getDate()).slice(-2),
          year: `${this.future.getFullYear()}`,
          month: monthNames[this.future.getMonth()].substr(0, 3)
        };
        if (this.subscription) {
          this.subscription.unsubscribe();
        }
      }
      this.diff = Math.floor((+this.future - new Date().getTime()) / 1000);
      this.progress = this.diff * 1000 / (this.futureNumber - this.beginningNumber) * this.fullProgress;
      return x;
    };

    this.updateDates();
    this.$counter = Observable.interval(1000).map(timerTick);
    this.subscription = this.$counter.subscribe((x) => this.timerData = this.countData(this.diff));
  }

  updateDates(start?, end?): void {
    this.lastDateEnd = end || this.dateEnd;
    this.beginning = new Date(start || this.dateStart);
    this.beginningNumber = +this.beginning;
    this.future = new Date(end || this.dateEnd);
    this.futureNumber = +this.future;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
