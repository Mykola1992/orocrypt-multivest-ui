import { Component, ElementRef, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
})

export class BalanceComponent implements OnInit {
  currency = 'CNAT';
  constructor(
    private elm: ElementRef
  ) { }

  @Input('amount') balance: 0;

  ngOnInit() { }
}
