import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, RoutesRecognized, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from '../shared';

@Component({
  selector: 'app-contribution',
  templateUrl: './contribution.component.html',
  styleUrls: ['./contribution.component.scss']
})

export class ContributionComponent implements OnInit, OnDestroy {
  currentUser;
  progressBar = 0;
  dateStart = 0;
  dateEnd = 0;
  subscriptions = [];
  pageState: string;
  pageSubtype: string;
  tradingStatus: string; // [soon, started, ended]
  cryptoNames = [];
  cryptoList;
  pageTitle: string;
  pageText: string;
  tokensForm: FormGroup;
  selectedToken;
  formFields;
  stats;
  ethWallet;
  walletData;
  userBalance = {};
  toPayTimeout;
  toPayLastValue;
  copiedHints;
  savingFiatStats = false;
  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.copiedHints = {};
    this.stats = {};
    this.subscriptions.push(router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.pageTitle = '';
        this.pageText = '';
          this.formFields = {};
        this.pageState = this.route.snapshot.data.type;
        const children = this.route.snapshot.children[0];
        this.pageSubtype = children && children.data.type;
        if (this.pageSubtype === 'tokensForm') {
          delete this.pageState;
        }
        this.userService.getStats().subscribe(stats => {
          this.stats = stats;
          const startDate = new Date(+stats.startTime);
          // const startDate = new Date(+new Date() - 6000000);
          this.dateStart = +startDate + startDate.getTimezoneOffset() * 60000;
          const endDate = new Date(+stats.endTime);
          // const endDate = new Date(+new Date() + 90000000);
          this.dateEnd = +endDate + endDate.getTimezoneOffset() * 60000;
          const allTokens = stats.soldTokens + stats.tokensForSale;
          this.progressBar = stats.soldTokens / allTokens * 100;
          this.cryptoNames = [];
          Object.keys(stats).forEach(key => {
            if (!key.indexOf('formattedTokensPer')) {
              this.cryptoNames.push(key.substr(18));
            }
          });
          this.cryptoList = this.cryptoNames.map(el => ({
            name: el,
            value: +stats[`formattedTokensPer${el}`],
            count: stats.collected[el.toLowerCase()]
          }));
          const currentTime = +new Date();
          if (!this.currentUser.token) {
            this.pageTitle = 'Cryptonaut Token Sale';
            this.pageText = '';
          } else if (currentTime < +stats.startTime) {
            this.tradingStatus = 'soon';
            this.pageTitle = 'CNAT Token Sale Is Starting Soon';
            this.pageText = 'Purchase CNAT tokens using one of below methods';
          } else if (currentTime > +stats.startTime && currentTime < +stats.endTime) {
            this.tradingStatus = 'started';
            this.pageTitle = 'Contribution Tracker';
            this.pageText = 'Purchase CNAT tokens using one of below methods';
          } else {
            this.tradingStatus = 'ended';
            this.pageTitle = 'CNAT Token Sale Has Ended';
            this.pageText = 'Purchase CNAT tokens using one of below methods';
          }
          if (this.pageSubtype === 'buy') {
            this.pageTitle = 'Your Preferred Payment Method';
          } else if (this.pageSubtype === 'tokensForm') {
            this.ethWallet = '';
            this.walletData = '';
            this.selectedToken = children.params.tokens;
            this.userService.getAddresses(children.params.tokens.toLowerCase()).subscribe(data => {
              this.ethWallet = data.icoAddress;
              this.walletData = data.data;
            });
          } else if (this.pageSubtype === 'tokensSuccess') {
            this.pageTitle = 'Your CNAT is Being Prepared';
            this.pageText = '<span>Congratulations, your transaction was successful!</br></br>' +
              'Thank you for your participation!</br></br>' +
              'To receive your tokens please proceed the accreditation process clicking the button below.</br></br>' +
              'The Cryptonaut Team.</span>';
          } else if (this.pageSubtype === 'buyInfo') {
            this.pageTitle = 'Payment Instruction';
            this.pageText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo quam sit amet ' +
              'justo consectetur tempor et vitae mi. Nulla nibh erat, suscipit sit amet commodo et, ' +
              'lobortis id quam. Maecenas mattis sollicitudin mauris. ' +
              'Vivamus interdum porta nisl, eu dapibus ex tempor non. Proin nec pharetra dolor, ut efficitur ante.';
          }
        });
      }
    }));
  }

  ngOnInit() {
    this.formFields = {};
    this.tokensForm = this.fb.group({
      'amount': [{ value: '', disabled: true}],
      'toPay': [''],
      'ethWallet': [''],
      'walletData': ['']
    });

    // update user data
    if (this.userService.getCurrentUser().token) {
      this.userService.getProfile().subscribe();
      this.userService.getUserBalance().subscribe(balance => {
        this.userBalance = balance;
      });
    }
    this.subscriptions.push(this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    ));
  }

  updateAmount(type, event) {
    if (!this.selectedToken || !this.formFields.toPay || this.toPayLastValue === this.formFields.toPay) {
      return;
    }
    this.toPayLastValue = this.formFields.toPay;
    this.formFields.amount = '';
    if (this.toPayTimeout) {
      clearTimeout(this.toPayTimeout);
    }
    this.toPayTimeout = setTimeout(() => {
      this.userService.getTokensPerValue(this.formFields.toPay, this.selectedToken.toLowerCase())
        .subscribe(tokens => {
          this.formFields.amount = tokens;
        });
    }, 500);
  }

  copyToClipboard(element) {
    const copy = document.getElementById(element);
    copy['select']();
    try {
      const successful = document.execCommand('copy');
      const msg = successful ? 'successful' : 'unsuccessful';
    } catch (err) {
      console.log('Oops, unable to copy');
    }
    window.getSelection().removeAllRanges();
    if (this.copiedHints[element]) {
      clearTimeout(this.copiedHints[element]);
    }
    this.copiedHints[element] = setTimeout(() => {
      delete this.copiedHints[element];
    }, 5000);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
