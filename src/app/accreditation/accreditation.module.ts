import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AccreditationEditComponent } from './accreditation-edit.component';
import { AccreditationComponent } from './accreditation.component';
import { SharedModule, AuthGuard } from '../shared';
import { SelectModule } from 'ng2-select';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

const accreditationRouting: ModuleWithProviders = RouterModule.forChild([{
  path: 'accreditation',
  component: AccreditationComponent,
  canActivate: [AuthGuard]
}, {
  path: 'accreditation/edit',
  component: AccreditationEditComponent,
  canActivate: [AuthGuard]
}]);

@NgModule({
  imports: [
    accreditationRouting,
    SharedModule,
    SelectModule,
    FormsModule,
    TextMaskModule
  ],
  declarations: [
    AccreditationComponent,
    AccreditationEditComponent
  ],
  providers: [
    AuthGuard
  ]
})
export class AccreditationModule { }
