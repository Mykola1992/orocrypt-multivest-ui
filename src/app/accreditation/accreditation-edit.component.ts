import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/mergeMap';

import { UserService, SettingsService } from '../shared';

@Component({
  selector: 'app-contribution',
  templateUrl: './accreditation-edit.component.html',
  styleUrls: ['./accreditation-edit.component.scss']
})
export class AccreditationEditComponent implements OnInit, OnDestroy {
  isSubmitting = false;
  currentUser;
  accreditationForm: FormGroup;
  serverErrorsCount;
  formFields;
  serverErrors;
  countriesItems: Array<any>;
  USStates: Array<any>;
  CAStates: Array<any>;
  birthdayMask: Array<any> = [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
  subscriptions = [];
  profile;
  documentTypesItems: Array<any> = [{
    text: 'Passpord',
    id: 'Passpord'
  }, {
    text: 'Driver ID',
    id: 'DriverID'
  }, {
    text: 'ID Card',
    id: 'IDCard'
  }];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private settingsService: SettingsService,
    private fb: FormBuilder,
  ) {
    // use FormBuilder to create a form group
    this.accreditationForm = this.fb.group({
      'firstName': ['', Validators.required],
      'lastName': ['', Validators.required],
      'middleName': [''],
      'dateOfBirth': ['', Validators.required],
      'phoneNumber': ['', Validators.required],
      'gender': ['', Validators.required],
      'country': [''],
      'zipCode': ['', Validators.required],
      'city': ['', Validators.required],
      'state': [''],
      'street': ['', Validators.required],
      'appartment': [''],
      'ETHWallet': ['', Validators.required]
    });
  }

  ngOnInit() {
    this.formFields = {
      gender: 'Male'
    };
    this.serverErrors = {};
    this.subscriptions.push(this.userService.currentUser.subscribe((userData) => {
      this.profile = userData.profile || {};
      this.currentUser = userData;
      if (userData.profile && userData.profile.firstName) {
        this.accreditationForm.addControl('documentType', new FormControl('', Validators.required));
        this.accreditationForm.addControl('documentScan', new FormControl('', Validators.required));
        this.accreditationForm.addControl('residenceProof', new FormControl('', Validators.required));
        this.accreditationForm.addControl('selfie', new FormControl('', Validators.required));
      }
    }));

    this.subscriptions.push(this.userService.getProfile().subscribe(profileData => {
      this.formFields = Object.assign(this.formFields, profileData.profile);
      this.settingsService.settings.subscribe(settingsData => {
        if (settingsData.countries) {
          this.countriesItems = settingsData.countries;
          // todo: check if it needs
          this.USStates = settingsData.USStates;
          this.CAStates = settingsData.CAStates;
          if (profileData.profile.country === 'us') {
            this.formFields.activeState = this.USStates.find(el => el.id === this.formFields.state);
          } else if (profileData.profile.country === 'ca') {
            this.formFields.activeState = this.CAStates.find(el => el.id === this.formFields.state);
          }
          this.formFields.activeCountry = settingsData.countries.find(el => el.id === this.formFields.country);

          const documentType = this.documentTypesItems.find(el => el.id === this.formFields.documentType) ||
            this.documentTypesItems[0];
          this.formFields.activeDocumentType = {
            text: documentType.text,
            id: this.formFields.documentType
          };
          // scroll to 2nd form
          if (profileData.step1 === 'accepted') {
            setTimeout(() => {
              const element = window.document.getElementById('accreditationScrollBottom');
              if (element) {
                const rect = element.getBoundingClientRect();
                document.getElementsByTagName('HTML')[0].scrollTop = rect.top;
              }
            }, 100);
          }
        }
      });
    }));
  }

  submitForm() {
    this.isSubmitting = true;
    const credentials = this.accreditationForm.value;
    // fix selectors data!
    // todo: check this;
    credentials.country = credentials.country || this.countriesItems[0].id;
    credentials.documentType = credentials.documentType || this.documentTypesItems[0].id;
    Object.keys(credentials).forEach(key => {
      if (credentials[key] === undefined || credentials[key] === '') {
        delete credentials[key];
      }
    });

    this.userService
      .postProfile(credentials)
      .subscribe(
        data => {
          this.router.navigateByUrl('/accreditation');
        },
        err => {
          this.isSubmitting = false;
          if (err && err.errors) {
            Object.keys(err.errors).forEach(key => {
              this.serverErrors[key] = err.errors[key];
            });
          } else {
            this.serverErrors.general = 'Something went wrong, please try later!';
          }
        }
      );
  }

  updateFile(event, property) {
    this.formFields[property] = event;
    this.inputUpdates(property);
  }

  selected(value: any, property: string): void {
    this.formFields[property] = value.id;
  }

  inputUpdates(field) {
    delete this.serverErrors.general;
    if (this.serverErrors[field]) {
      delete this.serverErrors[field];
      this.serverErrorsCount = Object.keys(this.serverErrors).length;
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
