import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DocumentModalComponent } from '../shared/modals/document-modal.component';
import { DialogService } from 'ng2-bootstrap-modal';

import { UserService } from '../shared';

@Component({
  selector: 'app-contribution',
  templateUrl: './accreditation.component.html',
  styleUrls: ['./accreditation.component.scss']
})

export class AccreditationComponent implements OnInit, OnDestroy {
  currentUser;
  profile;
  subscriptions = [];
  constructor(
    private router: Router,
    private dialogService: DialogService,
    private userService: UserService,
  ) {

  }

  ngOnInit() {
    this.profile = {};
    this.userService.getProfile().subscribe(profileData => {
      this.profile = profileData.profile || {};
    });
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
  }

  showDocumentModal(type) {
    const obj = {
      title: type
    };
    this.subscriptions.push(this.dialogService.addDialog(DocumentModalComponent, obj).subscribe());
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
